module.exports = function (api) {
  const isTest = api?.env('test');
  const targets = isTest ? { node: 'current' } : '> 0.25%, not dead';

  return {
    presets: [
      [
        '@babel/preset-env',
        {
          targets,
        },
      ],
      '@babel/preset-react',
      '@babel/preset-typescript',
    ],
  };
};
