const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const { DefinePlugin } = require('webpack');
const { customAlphabet } = require('nanoid');

const nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', 10);

module.exports = function () {
  return {
    entry: {
      module: './src/index.ts',
      worker: './src/worker.ts',
    },
    devtool: 'source-map',
    output: {
      path: path.resolve(__dirname, 'build'),
      filename: '[name].js',
      library: {
        name: 'module',
        type: 'commonjs',
      },
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    externals: {
      react: 'commonjs react',
      sinon: 'sinon',
    },
    plugins: [
      new CopyPlugin({ patterns: [{ from: 'public' }] }),
      // This suffix is used in module name
      new DefinePlugin({ RANDOM_SUFFIX: JSON.stringify(nanoid()) }),
    ],
    module: {
      rules: [
        {
          test: /\.(tsx|ts)$/,
          include: path.resolve(__dirname, 'src'),
          exclude: [/node_modules/, new RegExp('.test.tsx?')],
          use: [
            {
              loader: 'babel-loader',
            },
          ],
        },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: {
                  auto: true,
                  mode: 'local',
                  localIdentName: '[name]__[local]--[hash:base64:5]',
                },
              },
            },
            'sass-loader',
          ],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif|mp4)$/i,
          type: 'asset/resource',
        },
      ],
    },
  };
};
