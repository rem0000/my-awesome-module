/*

This script generate a file with current git hash and build date in source directory.

This will allow to display the current version in UI.

*/

const {execSync} = require('child_process');
const fs = require('fs');
const path = require('path');
const hasbin = require('hasbin');

main().catch(err => {
  console.error(err);
  process.exit(1);
})

async function main() {
  const buildInfo = {
    date: new Date().toISOString(),
    hash: '<no-version>',
  };

  // Inject git hash if possible
  try {
    if (await hasBinary('git')) {
      buildInfo.hash = execSync('git rev-parse HEAD', {stdio: "pipe"}).toString('utf-8').substring(0, 10);
    }
  } catch (err) {
    console.error('git error: ', err && err.message);
  }

  // Write file
  const filePath = path.resolve(process.cwd(), 'src', 'version.ts');
  fs.writeFileSync(filePath, fileTemplate(buildInfo), {encoding: 'utf-8'});

  console.log(`Wrote version ${buildInfo.hash} to ${filePath}`);
}

function fileTemplate(buildInfo) {
  return `\
/* eslint-disable */
// WARNING: THIS FILE IS GENERATED, DO NOT MANUALLY EDIT
export const VERSION = ${JSON.stringify(buildInfo, null, 4)};
`;
}

function hasBinary(name) {
  return new Promise((resolve) => hasbin(name, (result) => resolve(result)));
}
