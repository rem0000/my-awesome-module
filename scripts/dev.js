/**
 *  This script is used for development.
 *
 *  It will watch your code then transpile it with webpack, and simultaneous serve it on locahost.
 */
const concurrently = require('concurrently');
const open = require('open');

const { result } = concurrently(['yarn:watch', 'yarn:serve'], { killOthers: ['success', 'failure'] });

const openTimeout = setTimeout(() => open('http://localhost:7000'), 1000);

result.catch(() => {
  console.error('Startup failed.');
  clearTimeout(openTimeout);
  process.exit(1);
});
