#!/usr/bin/env sh

# This script performs all the necessary steps to deploy this module on Gitlab Pages

set -e

yarn install --frozen-lockfile
yarn run build

rm -rf public
mv build public
