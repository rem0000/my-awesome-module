#!/usr/bin/env sh

# This script performs all the necessary steps to build and test this module.
# You can use it in continuous integration setups.

set -e

yarn install --frozen-lockfile
yarn run lint
yarn run build
yarn run test

echo "It works 💪 (hope so)"
