/**
 * This script is the development server. It will use localhost:7000
 */
const fastify = require('fastify')();
const path = require('path');
const fastifyStatic = require('fastify-static');
const fastifyCors = require('fastify-cors');

const build = path.resolve(__dirname, '..', 'build');
const publicDir = path.resolve(__dirname, '..', 'public');

fastify.register(fastifyCors, { origin: true });

fastify.register(fastifyStatic, { root: [publicDir, build] });

fastify.listen(process.env.PORT || 7000, function (err, address) {
  if (err) {
    console.error('Cannot start server: ', err);
    process.exit(1);
  }

  console.log(`Serving files from ${build}, ${publicDir}.`);
  console.log(`Use URL ${address}`);
});
