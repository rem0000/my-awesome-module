<img alt="Abc-Map" src="./public/abc-map-logo.png" style="width: 70px"/>

# Abc-Map: module template

[![pipeline status](https://gitlab.com/abc-map/module-template/badges/master/pipeline.svg)](https://gitlab.com/abc-map/module-template/-/commits/master)

- [What is this repository for ?](#what-is-this-repository-for-)
- [Getting started](#getting-started)
- [File structure](#file-structure)
- [About testing](#about-testing)
- [About workers](#about-workers)
- [Deployment on Gitlab](#deployment-on-gitlab)
- [What do I need to know to write a module using this template ?](#what-do-i-need-to-know-to-write-a-module-using-this-template-)
- [TODO](#todo)

## What is this repository for ?

This is a template for creating Abc-Map modules. You can try it here: [https://abc-map.gitlab.io/module-template/](https://abc-map.gitlab.io/module-template/)

Using this template you can add your own features to [Abc-Map](https://abc-map.fr) using Javascript or Typescript.

**Why should I create an Abc-Map module ?**

- It is relatively simple to write: using this template you just have to implement your feature
- It will be easily accessible to users: you can share your modules thanks to Gitlab pages and GitHub pages

See [documentation](https://gitlab.com/abc-map/abc-map_private/-/blob/master/documentation/6_modules.md) for more
information.

## Getting started

You must install NodeJS 14 or above and Yarn. You can use [NVM](https://github.com/nvm-sh/nvm):

```
  $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
  $ bash
  $ nvm install 14
  $ npm install --global yarn
```

Initialize your own module:

```
  $ npx -p @abc-map/create-module create-module --name my-module
```

Start:

```
  $ yarn
  $ yarn dev
```

## File structure

- `src`: Where your source code is written
- `src/ModuleUI.tsx`: User interface
- `src/Module.tsx`: The logic of your module, where you process features or do whatever you want
- `src/worker`: A backend process, you should use it for long or resource consuming operations
- `build`: The content of your module, the files you can distribute

## About testing

You can do tests to increase the quality of your module.

If you don't want to test your code, you can safely delete all files named '\*.test.ts'.

## About workers

You can use workers for heavy computation tasks. You can use two methods:

- remoteWorkerLoader(): Load a single worker and return a reference to it
- remoteWorkerPoolLoader(): Load a pool of worker and return a method to execute tasks

## Deployment on Gitlab

- Create a new Gitlab repository
- Push your module to your repository
- Wait for the continuous integration pipeline
- Then go to Settings / Pages and copy your module URL, generally for example `https://abc-map.gitlab.io/module-template/`

Your module is deployed 🚀

## What do I need to know to write a module using this template ?

The short list:

- [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) and [Typescript](https://www.typescriptlang.org/)
- [Openlayers](https://openlayers.org/)

The real list:

- [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) and [Typescript](https://www.typescriptlang.org/)
- [Openlayers](https://openlayers.org/)
- [React](https://en.reactjs.org/)
- [Jest](https://jestjs.io/)
- [Web workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)
- [Sass](https://sass-lang.com/) / [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
- [Comlink](https://github.com/GoogleChromeLabs/comlink)
- Gitlab CI and [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/)

## TODO

- Explain tests and unit testing, why, how, ...
- Create a python template (https://pyodide.org/en/stable/)
- Create a webasm template
