import * as React from 'react';
import { Module as ModuleInterface, ModuleId, Logger } from '@abc-map/module-api';
import { ModuleUI } from './ModuleUI';
import { newDefaultParameters, Parameters, Result, BufferParameters, geometrySelectionToTypes, ProcessingProgressEvent } from './parameters';
import { translation } from './translations/i18n';
import { GeoJSON } from 'ol/format';
import { Feature } from '@turf/helpers';
import { remoteWorkerPoolLoader, WorkerPoolContext } from './utils/remoteWorkerPoolLoader';

export const logger = Logger.get('Module.ts', 'info');

const t = translation('Module:');

/**
 * This is a module. It can be loaded in an instance of Abc-Map.
 *
 * You can access map and various services, process data and eventually inject it back in map.
 *
 */
export class Module implements ModuleInterface {
  // This is the parameters used for processing
  private parameters = newDefaultParameters();
  // Eventual last progress event
  private progress?: ProcessingProgressEvent;
  // Eventual last results
  private result?: Result;
  // This object will dispatch progress and result events
  private emitter = document.createDocumentFragment();

  private worker?: WorkerPoolContext;

  constructor(private workerLoader = remoteWorkerPoolLoader) {}

  /**
   * Module id identify the module, in lists and URLs.
   *
   * It must be stable. We recommend you to use a random suffix.
   */
  public getId(): ModuleId {
    return `example-module-${RANDOM_SUFFIX}`;
  }

  /**
   * Readable name will be displayed in list of module and on top of module.
   */
  public getReadableName(): string {
    return t('Create_buffers');
  }

  /**
   * This method returns the main user interface (UI), displayed in Abc-Map. User interface in module
   * is generally used to gather parameters for processing.
   */
  public getUserInterface() {
    return (
      <ModuleUI
        parameters={this.parameters}
        onChange={this.setParameters}
        onProcess={this.process}
        onCancel={this.handleCancel}
        progress={this.progress}
        result={this.result}
        emitter={this.emitter}
      />
    );
  }

  /**
   * This is where you should implement your module logic.
   *
   * In this example we read features from main map, and we send chunks of features to the worker.
   * The worker buffer them with turfjs, then we create a new vector layer and add it to the main map.
   *
   * For few features or rapid operations you can execute all your logic here, but
   * you should always delegate long and resource consuming tasks to the worker, see 'worker.ts'
   *
   */
  public process = async (): Promise<void> => {
    logger.info('Start processing using parameters:', this.parameters);
    const start = Date.now();
    const result: Result = { errors: [] };
    const { LayerFactory, FeatureWrapperFactory, services, mainMap: map } = moduleApi;
    const { layerId, geometrySelection } = this.parameters;

    // We check that selected layer exists and is a vector layer
    const layer = map.getLayers().find((layer) => layer.getId() === layerId);
    if (!layer || !layer.isVector()) {
      result.errors.push(t('You_must_choose_a_valid_vector_layer'));
      this.dispatchResult(result);
      return;
    }

    // We check that selected layer exists and is a vector layer
    const layerIsEmpty = !layer.getSource().getFeatures().length;
    if (layerIsEmpty) {
      result.errors.push(t('Selected_layer_does_not_contain_any_features'));
      this.dispatchResult(result);
      return;
    }

    // Worker expect data in EPSG:3857, so we load projection for future transforms
    const projection = 'EPSG:3857';
    await services.geo.loadProjection(projection);

    // We filter geometries based on their types
    const geometryTypes = geometrySelectionToTypes(geometrySelection);
    const filtered = layer
      .getSource()
      .getFeatures()
      .filter((feature) => feature.getGeometry() && geometryTypes.includes(feature.getGeometry()?.getType()));

    // We transform geometries to GeoJSON (EPSG:3857) for further buffer processing with turfjs
    const format = new GeoJSON();
    const collection = format.writeFeaturesObject(filtered, { dataProjection: layer.getProjection()?.name, featureProjection: projection });

    // We initialize a new layer
    const total = collection.features.length;
    const newLayer = LayerFactory.newVectorLayer();
    map.addLayer(newLayer);

    // We process data chunk by chunk in a web worker, in order to not freeze UI and display progress state
    this.worker = await this.workerLoader();
    const chunkSize = 2;
    const tasks: Promise<void>[] = [];
    for (let i = 0; i < total; i += chunkSize) {
      // We create a chunk
      const chunk: BufferParameters = {
        ...this.parameters,
        collection: {
          ...collection,
          features: collection.features.slice(i, i + chunkSize) as Feature[],
        },
      };

      // We create buffers, then prepare them for display in Abc-Map
      tasks.push(
        this.worker
          .execute((remote) => remote.createBuffers(chunk))
          .then((buffered) => {
            const projected = format.readFeatures(buffered, { dataProjection: projection, featureProjection: layer.getProjection()?.name });
            const styled = projected.map((feat) => FeatureWrapperFactory.from(feat).setId().setDefaultStyle().unwrap());
            newLayer.getSource().addFeatures(styled);

            this.dispatchProgress({ total, current: newLayer.getSource().getFeatures().length });
          })
      );
    }

    // We wait until all tasks done
    await Promise.all(tasks);

    // End of processing, we add layer
    this.dispatchProgress({ total, current: total });

    logger.info(`Processing took ${Math.round((Date.now() - start) / 100) / 10}s`);
    this.dispatchResult(result);
  };

  private handleCancel = () => {
    this.progress = undefined;
    this.worker?.dispose();
  };

  private dispatchProgress(ev: ProcessingProgressEvent) {
    this.progress = ev;
    this.emitter.dispatchEvent(new CustomEvent('progress', { detail: this.progress }));
  }

  private dispatchResult(result: Result) {
    this.result = result;
    this.emitter.dispatchEvent(new CustomEvent('result', { detail: this.result }));
  }

  public setParameters = (parameters: Parameters) => {
    this.parameters = parameters;
  };

  public getProgressEmitter(): EventTarget {
    return this.emitter;
  }
}
