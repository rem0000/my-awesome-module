import { FeatureCollection } from '@turf/helpers';
import GeometryType from 'ol/geom/GeometryType';

/**
 * These parameters are needed for processing
 */
export interface Parameters {
  layerId: string;
  bufferSize: number;
  units: 'meters' | 'kilometers';
  geometrySelection: GeometrySelection[];
}

export type GeometrySelection = 'points' | 'lines' | 'polygons';

/**
 * This function return the default parameters, used if no parameters selected
 */
export function newDefaultParameters(): Parameters {
  return {
    layerId: '',
    bufferSize: 15,
    units: 'kilometers',
    geometrySelection: ['points', 'lines', 'polygons'],
  };
}

/**
 * When processing done, module we return this object to UI
 */
export interface Result {
  errors: string[];
}

/**
 * Using the worker, you can pass progress events to UI in order to inform user.
 */
export interface ProcessingProgressEvent {
  total: number;
  current: number;
}

/**
 * Worker processing internal parameters
 */
export interface BufferParameters {
  bufferSize: number;
  units: 'meters' | 'kilometers';
  collection: FeatureCollection;
}

export function geometrySelectionToTypes(selection: GeometrySelection[]): string[] {
  const result: string[] = [];
  if (selection.includes('points')) {
    result.push(GeometryType.POINT, GeometryType.MULTI_POINT);
  }
  if (selection.includes('lines')) {
    result.push(GeometryType.LINE_STRING, GeometryType.MULTI_LINE_STRING);
  }
  if (selection.includes('polygons')) {
    result.push(GeometryType.POLYGON, GeometryType.MULTI_POLYGON);
  }

  return result;
}
