import React from 'react';
import Styles from './Loader.module.scss';
import clsx from 'clsx';

interface Props {
  progress?: ProgressValues;
  className?: string;
}

interface ProgressValues {
  current: number;
  total: number;
}

function Loader(props: Props) {
  const { progress, className } = props;

  return (
    <div className={clsx(Styles.loader, className)}>
      <div className={Styles.animation}>
        <div className={Styles.inner}>
          <div className={Styles.content}>
            <span className={Styles.spinner}></span>
          </div>
        </div>
      </div>

      {progress && (
        <div className={Styles.progress}>
          {Math.round((progress.current / progress.total) * 100) || 0} % ({progress.current} / {progress.total})
        </div>
      )}
    </div>
  );
}

export default Loader;
