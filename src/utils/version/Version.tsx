import Styles from './Version.module.scss';
import * as React from 'react';
import { VERSION } from '../../version';
import { DateTime } from 'luxon';
import { useTranslation } from 'react-i18next';

function Version() {
  const { t } = useTranslation('Version');
  return (
    <div className={Styles.version}>
      {t('Built_on')} {DateTime.fromISO(VERSION.date).toLocaleString(DateTime.DATETIME_SHORT)}
      {VERSION.hash && <>- {VERSION.hash.slice(0, 20)}</>}
    </div>
  );
}

export default Version;
