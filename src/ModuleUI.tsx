import Styles from './ModuleUI.module.scss';
import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import Illustration from './assets/screenshot.png';
import { Parameters, ProcessingProgressEvent, Result } from './parameters';
import { useNoobFormBuilder, Language, Logger } from '@abc-map/module-api';
import { useTranslation } from 'react-i18next';
import { useLanguageUpdate } from './utils/useLanguageUpdate';
import { FormErrors } from '@abc-map/module-api';
import Version from './utils/version/Version';
import Loader from './utils/loader/Loader';
import clsx from 'clsx';

const logger = Logger.get('ModuleUI.tsx');

interface Props {
  parameters: Parameters;
  onChange: (p: Parameters) => void;
  onProcess: () => Promise<void>;
  onCancel: () => void;
  emitter: EventTarget;
  // Eventual previous progress event
  progress?: ProcessingProgressEvent;
  // Eventual previous result
  result?: Result;
  // Lang is injected at runtime
  lang?: Language;
}

export function ModuleUI(props: Props) {
  const { parameters, onChange, onProcess, onCancel, progress: initialProgress, result: initialResult, emitter, lang } = props;
  const [result, setResult] = useState<Result | undefined>(initialResult);
  const [progress, setProgress] = useState<ProcessingProgressEvent | undefined>(initialProgress);
  useLanguageUpdate(lang);

  // You can access main map containing project data from moduleApi.
  const { mainMap } = moduleApi;

  // Format your messages with t for internationalisation
  const { t } = useTranslation('Module');

  // You can build trivial forms with formBuilder, a simple helper built with https://react-hook-form.com/api/
  // Builder is completely optional, you can build your own form or use Formik or React Hooks Form
  const formBuilder = useNoobFormBuilder<Parameters>(parameters);

  // We update module parameters each time form changes
  useEffect(() => {
    const subscription = formBuilder.onChange((values) => onChange(values));
    return () => subscription.unsubscribe();
  }, [formBuilder, onChange]);

  // We trigger processing when user submit form
  const handleSubmit = useCallback(
    (values: Parameters) => {
      onChange(values);

      // We reset state
      setProgress({ total: 0, current: 0 });
      setResult(undefined);

      // Start processing
      onProcess().catch((err) => {
        logger.error('Processing error: ', err);
        setResult({ errors: [t('Unexpected_error')] });
      });
    },
    [onChange, onProcess, t]
  );
  formBuilder.onSubmit(handleSubmit);

  // Each time we receive progress event we update ui
  useEffect(() => {
    const handleProgress = (ev: CustomEvent<ProcessingProgressEvent>) => setProgress(ev.detail);
    emitter.addEventListener('progress', handleProgress as EventListener);

    const handleResult = (ev: CustomEvent<Result>) => setResult(ev.detail);
    emitter.addEventListener('result', handleResult as EventListener);

    return () => {
      emitter.removeEventListener('progress', handleProgress as EventListener);
      emitter.removeEventListener('result', handleResult as EventListener);
    };
  }, [emitter]);

  // Cancel processing when user wants to
  const handleCancel = useCallback(() => {
    onCancel();
    setProgress(undefined);
  }, [onCancel]);
  formBuilder.onCancel(handleCancel);

  // Handle form errors
  const handleErrors = useCallback(
    (errors: FormErrors<Parameters>) => {
      const messages: string[] = [];
      if (errors.layerId) {
        messages.push(t('Source_layer_is_mandatory'));
      }
      if (errors.bufferSize) {
        messages.push(t('The_buffer_size_must_be_between_10_and_1000'));
      }
      if (errors.units) {
        messages.push(t('Unit_is_mandatory'));
      }
      if (errors.geometrySelection) {
        messages.push(t('At_least_one_geometry_type_must_be_selected'));
      }
      return messages;
    },
    [t]
  );
  formBuilder.onErrors(handleErrors);

  // The parameters form, built with a form builder.
  const form = formBuilder
    .addLayerSelector(
      {
        name: 'layerId',
        label: t('Input_layer'),
      },
      mainMap,
      { required: true }
    )
    .addNumber(
      {
        name: 'bufferSize',
        label: t('Buffer_size'),
      },
      { min: 10, max: 1000 }
    )
    .addSelect(
      {
        name: 'units',
        label: t('Units'),
      },
      [
        { value: 'meters', label: t('Meters') },
        { value: 'kilometers', label: t('Kilometers') },
      ],
      { required: true }
    )
    .addCheckboxes(
      {
        name: 'geometrySelection',
        label: t('Geometry_types'),
      },
      [
        { value: 'points', label: t('Points') },
        { value: 'lines', label: t('Lines') },
        { value: 'polygons', label: t('Polygons') },
      ],
      { validate: (value) => !!(Array.isArray(value) && value.length) }
    )
    .setCommonLabels({ submitButton: t('Submit'), formHasErrors: t('Form_has_errors') })
    .build();

  // To facilitate creation of interfaces, you can use Bootstrap css without adding dependencies,
  // or anything else you want to bundle in your module
  return (
    <div className={clsx(Styles.module, 'container')}>
      <div className={'row'}>
        {/* Main form.*/}
        <div className={'col-md-6'}>
          <div className={Styles.introduction}>{t('This_module_allows_to_create_buffers')}</div>

          <div className={'mb-3'}>{form}</div>

          {/* Progress */}
          {!!progress && !result && <Loader progress={progress} className={'mb-3'} />}

          {/* Results and errors */}
          {result && (
            <>
              {/* Errors while processing */}
              {result.errors.length > 0 && (
                <div className={'mt-3'}>
                  {t('Errors')}:
                  <div>
                    {result.errors.map((err) => (
                      <div key={err}>{err}</div>
                    ))}
                  </div>
                </div>
              )}

              {/* End of processing */}
              {result.errors.length < 1 && <div>{t('Processing_done')}</div>}
            </>
          )}
        </div>

        {/* Introduction and illustrations */}
        <div className={'col-md-6'}>
          <img src={Illustration} alt={'Illustration'} />
          <i className={'m-3'}>{t('Example_of_output')}</i>
        </div>
      </div>

      <Version />
    </div>
  );
}
