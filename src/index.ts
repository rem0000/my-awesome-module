// We need this declaration before any imports
import '@abc-map/module-api/build/public-path-setup';
import './translations/i18n';
import { Logger, ModuleFactory } from '@abc-map/module-api';
import { Module } from './Module';

const logger = Logger.get('index.ts', 'info');

/**
 * This is the entrypoint of module. When module will be loaded, this function will be executed.
 *
 * It must return a module instance.
 */
const moduleFactory: ModuleFactory = function () {
  const module = new Module();
  logger.info('Module loaded: ' + module.getId(), module);
  return module;
};

export default moduleFactory;
