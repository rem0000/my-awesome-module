# assets

This directory contains assets that will be integrated into your module (images, sounds, videos).

You can import files from this directory directly into your JavaScript code.

See the `webpack.config.js` file and the documentation [webpack 5](https://webpack.js.org/).
