import { ModuleWorker } from './worker';
import { sampleFeatureCollection } from './__fixtures__/sampleFeatureCollection';

/**
 * This is a test file.
 *
 * If you do not want to test your code, you can remove this file safely.
 */
describe('worker', function () {
  let worker: ModuleWorker;

  beforeEach(() => {
    worker = new ModuleWorker();
  });

  it('buffer()', () => {
    const buffered = worker.createBuffers({
      collection: sampleFeatureCollection(),
      bufferSize: 15,
      units: 'kilometers',
    });

    expect(buffered).toMatchSnapshot();
  });
});
