import { logger, Module } from './Module';
import { initTestModuleApi, newTestFeatureWrapper, newTestLayerWrapper, TestModuleApi } from '@abc-map/module-api';
import { Parameters, ProcessingProgressEvent, Result } from './parameters';
import * as sinon from 'sinon';
import { sampleOpenlayersFeatures } from './__fixtures__/sampleOpenlayersFeatures';
import VectorSource from 'ol/source/Vector';
import { Feature } from 'ol';
import { testRemoteWorkerPoolLoader, TestWorkerPoolContext } from './utils/testRemoteWorkerPoolLoader';

logger.disable();

/**
 * This is a test file.
 *
 * If you do not want to test your code, you can remove this file safely.
 */
describe('Module', function () {
  let moduleApi: TestModuleApi;
  let pool: TestWorkerPoolContext;
  let module: Module;

  beforeEach(() => {
    moduleApi = initTestModuleApi();

    const [context, loader] = testRemoteWorkerPoolLoader();
    pool = context;

    module = new Module(loader);
  });

  it('getReadableName()', () => {
    expect(module.getReadableName()).toEqual('Create buffers (Module template)');
  });

  it('process()', async () => {
    // Prepare
    // We create a fake source layers with 35 features
    const sourceLayer = newTestLayerWrapper();
    sourceLayer.getId.returns('test-layer-id');
    sourceLayer.isVector.returns(true);

    const sourceFeatures = sampleOpenlayersFeatures(35);
    const source = sinon.createStubInstance(VectorSource);
    source.getFeatures.returns(sourceFeatures);
    sourceLayer.getSource.returns(source);

    // We program the main map to return two layers including the one that must be processed
    moduleApi.mainMap.getLayers.returns([newTestLayerWrapper(), sourceLayer]);

    // Calls to FeatureWrapperFactory will return one fake FeatureWrapper
    // This allows to control that all new features have been correctly prepared
    const featureWrapper = newTestFeatureWrapper();
    featureWrapper.unwrap.returns(new Feature());
    moduleApi.FeatureWrapperFactory.from.returns(featureWrapper);

    // Calls to LayerFactory.newVectorLayer() will return one fake LayerWrapper
    const destinationLayer = newTestLayerWrapper();
    const destinationSource = sinon.createStubInstance(VectorSource);
    destinationSource.getFeatures.returns([]);
    destinationLayer.getSource.returns(destinationSource);
    moduleApi.LayerFactory.newVectorLayer.returns(destinationLayer);

    // Processing parameters
    const parameters: Parameters = {
      layerId: 'test-layer-id',
      bufferSize: 15,
      units: 'kilometers',
      geometrySelection: ['points', 'lines', 'polygons'],
    };
    module.setParameters(parameters);

    // Worker will return features
    pool.worker.createBuffers.callsFake((params) => params.collection);

    const progressListener = sinon.stub<[CustomEvent<ProcessingProgressEvent>], void>();
    module.getProgressEmitter().addEventListener('progress', progressListener as any);

    const resultListener = sinon.stub<[CustomEvent<Result>], void>();
    module.getProgressEmitter().addEventListener('result', resultListener as any);

    // Act
    await module.process();

    // Assert
    // We expect that the progress has been correctly reported
    expect(progressListener.callCount).toEqual(19);
    expect(progressListener.args[0][0].detail).toEqual({ current: 0, total: 35 });
    expect(progressListener.args[18][0].detail).toEqual({ current: 35, total: 35 });

    // We expect that worker has been called correctly
    expect(pool.worker.createBuffers.callCount).toEqual(18);
    expect(pool.worker.createBuffers.args[0][0].bufferSize).toEqual(15);
    expect(pool.worker.createBuffers.args[0][0].units).toEqual('kilometers');

    expect(pool.worker.createBuffers.args[0][0].collection.features[0].id).toEqual(0);
    expect(pool.worker.createBuffers.args[0][0].collection.features[1].id).toEqual(1);
    expect(pool.worker.createBuffers.args[17][0].collection.features[0].id).toEqual(34);

    // All features must have an id and must be styled, then added to source
    expect(featureWrapper.setId.callCount).toEqual(35);
    expect(featureWrapper.setDefaultStyle.callCount).toEqual(35);
    expect(destinationSource.addFeatures.callCount).toEqual(18);

    // No errors must be returned
    expect(resultListener.callCount).toEqual(1);
    expect(resultListener.args[0][0].detail).toEqual({ errors: [] });
  });
});
