import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import en from './en.json';
import fr from './fr.json';
import { Language, Logger } from '@abc-map/module-api';

const logger = Logger.get('i18n.tsx');

const resources = {
  [Language.English]: en,
  [Language.French]: fr,
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    detection: {
      order: ['path', 'localStorage', 'navigator'],
      lookupLocalStorage: 'ABC_LANGUAGE',
    },
    resources,
    supportedLngs: Object.values(Language),
    fallbackLng: Language.English,
    interpolation: {
      escapeValue: false,
    },
  })
  .catch((err) => logger.error('i18n init error:', err));

export interface StringMap {
  [k: string]: string | number;
}

export function translation(prefix: string) {
  return (key: string, params?: StringMap) => i18n.t(`${prefix}${key}`, params);
}

export function getLang(): Language {
  return i18n.language as Language;
}

export async function setLang(lang: Language): Promise<void> {
  await i18n.changeLanguage(lang);
}
