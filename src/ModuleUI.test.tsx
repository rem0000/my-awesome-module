import * as React from 'react';
import { initTestModuleApi, newTestLayerWrapper, TestModuleApi } from '@abc-map/module-api';
import { render } from '@testing-library/react';
import { newDefaultParameters, Parameters } from './parameters';
import * as sinon from 'sinon';
import { SinonStub } from 'sinon';
import userEvent from '@testing-library/user-event';
import { ModuleUI } from './ModuleUI';
jest.mock('./utils/version/Version', () => () => 'Version mock');

/**
 * This is a test file.
 *
 * If you do not want to test your code, you can remove this file safely.
 */
describe('ModuleUI', () => {
  let moduleApi: TestModuleApi;
  let parameters: Parameters;
  let onChange: SinonStub;
  let onProcess: SinonStub;
  let onCancel: SinonStub;

  beforeEach(() => {
    moduleApi = initTestModuleApi();

    parameters = newDefaultParameters();
    onChange = sinon.stub();
    onProcess = sinon.stub();
    onCancel = sinon.stub();
  });

  it('should submit values', async () => {
    // Prepare
    const layer1 = newTestLayerWrapper();
    layer1.getId.returns('test-layer-1-id');
    layer1.getName.returns('Test layer 1');
    moduleApi.mainMap.getLayers.returns([layer1]);

    onProcess.resolves();

    const testEmitter = document.createDocumentFragment();

    const { container, getByTestId } = render(
      <ModuleUI parameters={parameters} onChange={onChange} onProcess={onProcess} onCancel={onCancel} emitter={testEmitter} />
    );

    // Act
    await userEvent.selectOptions(getByTestId('layerId'), 'Test layer 1');
    await userEvent.clear(getByTestId('bufferSize'));
    await userEvent.type(getByTestId('bufferSize'), '150');
    await userEvent.selectOptions(getByTestId('units'), 'Meters');
    await userEvent.click(getByTestId('geometrySelection-points'));
    await userEvent.click(getByTestId('submit-button'));

    // Assert
    expect(container).toMatchSnapshot();
    expect(onChange.callCount).toEqual(10);
    expect(onChange.args[8][0]).toEqual({ bufferSize: '150', geometrySelection: ['lines', 'polygons'], layerId: 'test-layer-1-id', units: 'meters' });
    expect(onProcess.callCount).toEqual(1);
  });
});
