import * as Comlink from 'comlink';
import { BufferParameters } from './parameters';
import buffer from '@turf/buffer';
import { toMercator, toWgs84 } from '@turf/projection';
import { FeatureCollection } from '@turf/helpers';
import { Logger } from '@abc-map/module-api/build/utils/';

const logger = Logger.get('ModuleWorker.ts');

/**
 * You should always delegate long and resource consuming tasks to the worker.
 *
 * It allows to keep a responsive user interface.
 */
export class ModuleWorker {
  /**
   * Because this worker uses turfjs, all data in input must use EPSG:3857
   * @param parameters
   */
  public createBuffers(parameters: BufferParameters): FeatureCollection {
    const start = Date.now();
    const { bufferSize, units, collection: sourceCollection } = parameters;

    const bufferSizeKm = units === 'kilometers' ? bufferSize : bufferSize / 1000;

    const featuresWGS84 = {
      ...sourceCollection,
      features: sourceCollection.features.map((f) => toWgs84(f, { mutate: true })),
    };

    const buffered = buffer(featuresWGS84, bufferSizeKm, { units: 'kilometers', steps: 4 });

    const result = toMercator(buffered, { mutate: true });

    logger.info(`Took ${Math.round((Date.now() - start) / 100) / 10}`);
    return result;
  }
}

Comlink.expose(new ModuleWorker());
