/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

module.exports = {
  testEnvironment: 'jsdom',
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: ['/node_modules/'],
  coverageProvider: 'babel',
  coverageReporters: ['text', 'html'],
  errorOnDeprecated: true,
  roots: ['src'],
  transformIgnorePatterns: ['node_modules/(?!(ol|geotiff|@abc-map/*)/)'],
  moduleNameMapper: {
    // We mock assets
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/src/__mocks__/fileMock.js',
    '\\.(s?css|less)$': '<rootDir>/src/__mocks__/styleMock.js',
    // We ensure that only one version of react is used
    '^react$': '<rootDir>/node_modules/react',
  },
};
